import { expect } from 'chai';

describe("Hello World Test", () => {
    it("2 + 2 = 4", () => {
        expect(2 + 2).to.be.equal(4);
    })
})